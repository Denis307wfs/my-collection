package com.shpp.p2p.cs.dstepanov;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Iterator;


/**
 * Class for testing {@link MyLinkedList} by JUnit 5.
 */
public class TestMyLinkedList {
    private MyList<Integer> myList;

    @BeforeAll
    static void startMessage() {
        System.out.println("MyLinkedList tests started");
        System.out.println("----------------");
    }

    @AfterAll
    static void endMessage() {
        System.out.println("MyLinkedList tests ended");
    }

    @BeforeEach
    void createInstance() {
        myList = new MyLinkedList<>();
        myList.add(34);
        myList.add(12);
        myList.add(9);
    }

    @Test
    void addAndGet() {
        myList.add(1);
        myList.add(2);
        myList.add(8);
        List<Integer> list = new ArrayList<>(Arrays.asList(34, 12, 9, 1, 2, 8));
        Assertions.assertIterableEquals(list, myList);
        Assertions.assertEquals(34, myList.get(0));
        Assertions.assertEquals(8, myList.get(5));
        Assertions.assertEquals(1, myList.get(3));
    }

    @Test
    void addStringType() {
        MyList<String> myStringList = new MyLinkedList<>();
        myStringList.add("One");
        myStringList.add("Two");
        myStringList.add("Three");
        Assertions.assertEquals(3, myStringList.size());
    }

    @Test
    void getFirst() {
        myList.set(0, 24);
        myList.add(1, 98);
        myList.add(66);
        Assertions.assertEquals(24, myList.getFirst());
    }

    @Test
    void getLast() {
        myList.add(22);
        myList.add(1, 98);
        Assertions.assertEquals(22, myList.getLast());
    }

    @Test
    void getFirstOrLastWhenEmpty() {
        myList.clear();
        NoSuchElementException thrown = Assertions.assertThrows(
                NoSuchElementException.class, () -> myList.getFirst());
        Assertions.assertEquals("List is empty", thrown.getMessage());
    }

    @Test
    void size() {
        myList.add(2, 11);
        myList.add(19);
        Assertions.assertEquals(5, myList.size());
    }

    @Test
    void addWrongType() {
        NumberFormatException thrown = Assertions.assertThrows(
                NumberFormatException.class, () -> myList.add(Integer.valueOf("w")));
        Assertions.assertEquals("For input string: \"w\"", thrown.getMessage());
    }

    @Test
    void addWithWrongIndex() {
        myList.add(2, 11);
        myList.add(19);
        Assertions.assertEquals(5, myList.size());
        IndexOutOfBoundsException thrown = Assertions.assertThrows(
                IndexOutOfBoundsException.class, () -> myList.add(6, 0));
        Assertions.assertEquals("Index 6 out of bounds for length 6", thrown.getMessage());
    }

    @Test
    void set() {
        myList.set(1, 5);
        Assertions.assertEquals(5, myList.get(1));
    }

    @Test
    void remove() {
        myList.remove(1);
        Assertions.assertEquals(2, myList.size());
    }

    @Test
    void contains() {
        Assertions.assertTrue(myList.contains(12));
    }

    @Test
    void isEmpty() {
        Assertions.assertFalse(myList.isEmpty());
        myList.clear();
        Assertions.assertTrue(myList.isEmpty());
    }

    @Test
    void clear() {
        Assertions.assertFalse(myList.isEmpty());
        myList.clear();
        Assertions.assertEquals(0, myList.size());
        Assertions.assertTrue(myList.isEmpty());
    }

    @Test
    void iterator() {
        Iterator<Integer> iterator = myList.iterator();
        Assertions.assertTrue(iterator.hasNext());
        Assertions.assertEquals(34, iterator.next());
        Assertions.assertEquals(12, iterator.next());
        Assertions.assertTrue(iterator.hasNext());
        Assertions.assertEquals(9, iterator.next());
        Assertions.assertFalse(iterator.hasNext());
    }
}
