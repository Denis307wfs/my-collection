package com.shpp.p2p.cs.dstepanov;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;


/**
 * {@link MyArrayList} is an implementation of {@link MyList} interface.
 * This is a resizable data structure based on an array.
 *
 * @author Denys Stepanov
 */
public class MyArrayList<E> implements MyList<E> {
    /**
     * Default initial capacity
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * {@link MyArrayList} elements are stored here
     */
    private Object[] elements;
    /**
     * The number of elements in the {@link MyArrayList}
     */
    private int size;

    /**
     * This constructor creates an instance of {@link MyArrayList} with a specific capacity of an array inside.
     *
     * @param initCapacity - the initial capacity of the list
     * @throws IllegalArgumentException – if the specified initial capacity is negative or 0.
     */
    public MyArrayList(int initCapacity) throws IllegalArgumentException {
        if (initCapacity <= 0) throw new IllegalArgumentException();
        elements = new Object[initCapacity];
    }

    /**
     * This constructor creates an instance of {@link MyArrayList} with a default capacity of an array inside.
     * A default size of inner array is 10;
     */
    public MyArrayList() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Adds an element to the array.
     *
     * @param element element to add
     */
    @Override
    public void add(E element) {
        increaseSizeIfFullArray();
        elements[size] = element;
        size++;
    }

    /**
     * If the number of elements in the array == size then the array is doubled.
     */
    private void increaseSizeIfFullArray() {
        if (elements.length == size) {
            elements = Arrays.copyOf(elements, size * 2);
        }
    }

    /**
     * Adds an element to the specific position in the array
     *
     * @param index   index of position
     * @param element element to add
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    @Override
    public void add(int index, E element) {
        checkRangeOfArray(index);
        increaseSizeIfFullArray();
        System.arraycopy(elements, index, elements, index + 1, size - index);
        elements[index] = element;
        size++;
    }

    /**
     * The method checks if the index is not out of bounds, otherwise throws IndexOutOfBoundsException.
     *
     * @param index checked index
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    private void checkRangeOfArray(int index) {
        if (index > size || index < 0)
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds");
    }

    /**
     * Retrieves an element by its position index. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index index of element
     * @return en element
     */
    @Override
    public E get(int index) {
        Objects.checkIndex(index, size);
        return (E) elements[index];
    }

    /**
     * Returns the first element of the list. Operation is performed in constant time O(1)
     *
     * @return the first element of the list
     * @throws java.util.NoSuchElementException if list is empty
     */
    @Override
    public E getFirst() {
        if (isEmpty()) throw new NoSuchElementException("List is empty");
        return (E) elements[0];
    }

    /**
     * Returns the last element of the list. Operation is performed in constant time O(1)
     *
     * @return the last element of the list
     * @throws java.util.NoSuchElementException if list is empty
     */
    @Override
    public E getLast() {
        if (isEmpty()) throw new NoSuchElementException("List is empty");
        return (E) elements[size - 1];
    }

    /**
     * Changes the value of array at specific position. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index   position of value
     * @param element a new value
     */
    @Override
    public void set(int index, E element) {
        Objects.checkIndex(index, size);
        elements[index] = element;
    }

    /**
     * Removes an elements by its position index. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index element index
     * @return deleted element
     */
    @Override
    public E remove(int index) {
        Objects.checkIndex(index, size);
        E deleted = (E) elements[index];
        if (index < size - 1) {
            System.arraycopy(elements, index + 1, elements, index, size - index - 1);
        }
        elements[size - 1] = null;
        size--;
        return deleted;
    }

    /**
     * Checks for existing of a specific element in the list.
     *
     * @param element is element to find
     * @return If element exists method returns true, otherwise it returns false
     */
    @Override
    public boolean contains(E element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a list is empty
     *
     * @return {@code true} if list is empty, {@code false} otherwise
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns the number of elements in the list
     *
     * @return number of elements
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Removes all list elements
     */
    @Override
    public void clear() {
        elements = new Object[DEFAULT_CAPACITY];
        size = 0;
    }

    /**
     * Creates an instance of {@link Iter} who implements {@link Iterator}
     *
     * @return an instance of {@link Iter}
     */
    @Override
    public Iterator<E> iterator() {
        return new Iter();
    }

    /**
     * Implementation of interface {@link Iterator}
     */
    private class Iter implements Iterator<E> {
        int cursor;
        int lastRet = -1; // index of last element returned; -1 if no such

        /**
         * Checks for the existence of the next element
         */
        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        /**
         * Returns next element otherwise throw {@link NoSuchElementException}
         */
        @Override
        public E next() {
            if (isEmpty()) throw new NoSuchElementException("List is empty");
            if (cursor == size) throw new NoSuchElementException("No more elements");
            lastRet = cursor;

            return (E) elements[cursor++];
        }

        /**
         * Remove current element
         */
        @Override
        public void remove() {
            if (isEmpty()) throw new NoSuchElementException("List is empty");
            if (lastRet == size || lastRet < 0)
                throw new NoSuchElementException("No more elements or method called twice without next()");

            MyArrayList.this.remove(lastRet);
            cursor = lastRet;
            lastRet = -1;
        }
    }
}
