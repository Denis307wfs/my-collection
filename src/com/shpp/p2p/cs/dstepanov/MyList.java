package com.shpp.p2p.cs.dstepanov;


/**
 * The {@link MyList} is an ordered collection.
 *
 * @param <E> the type of elements in the list
 * @author Denys Stepanov
 */
public interface MyList<E> extends Iterable<E> {

    /**
     * The method adds element to the end of the list.
     *
     * @param element element to be added to the list
     */
    void add(E element);

    /**
     * The method adds element at specified index.
     *
     * @param index index to add
     * @param element element to be added to the list
     */
    void add(int index, E element);

    /**
     * Retrieves an element by its position index. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index index of element
     * @return en element
     */
    E get(int index);

    /**
     * Returns the first element of the list. Operation is performed in constant time O(1)
     * In case calls method when list is empty throws {@link java.util.NoSuchElementException}
     */
    E getFirst();

    /**
     * Returns the last element of the list. Operation is performed in constant time O(1)
     * In case calls method when list is empty throws {@link java.util.NoSuchElementException}
     */
    E getLast();

    /**
     * Changes the value of a list element at specific position. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index   a position of element to change
     * @param element a new element value
     */
    void set(int index, E element);

    /**
     * Removes an elements by its position index. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index element index
     * @return deleted element
     */
    E remove(int index);

    /**
     * Checks for existing of a specific element in the list.
     *
     * @param element is element to find
     * @return If element exists method returns true, otherwise it returns false
     */
    boolean contains(E element);

    /**
     * Checks if a list is empty
     *
     * @return {@code true} if list is empty, {@code false} otherwise
     */
    boolean isEmpty();

    /**
     * Returns the number of elements in the list
     *
     * @return number of elements
     */
    int size();

    /**
     * Removes all list elements
     */
    void clear();
}
