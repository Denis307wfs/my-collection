package com.shpp.p2p.cs.dstepanov;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;


/**
 * {@link MyLinkedList} is a list implementation that is based on singly linked generic nodes.
 * A node is implemented as inner static class {@link Node< E >}.
 *
 * @param <E> generic type parameter
 * @author Denys Stepanov
 */
public class MyLinkedList<E> implements MyList<E> {
    /**
     * The number of elements in {@link MyLinkedList}
     */
    private int size;
    /**
     * The first {@code Node} in the {@link MyLinkedList}
     */
    private Node<E> first;
    /**
     * The last {@code Node} in the {@link MyLinkedList}
     */
    private Node<E> last;


    /**
     * Adds an element to the end of the list.
     *
     * @param element element to add
     */
    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (isEmpty()) {
            first = last = newNode;
        } else {
            last.next = newNode;
            newNode.prev = last;
            last = newNode;
        }
        size++;
    }

    /**
     * Adds a new element to the specific position in the list.
     * In case provided index in out of the list bounds it throws {@link IndexOutOfBoundsException}
     *
     * @param index   an index of new element
     * @param element element to add
     */
    @Override
    public void add(int index, E element) {
        Objects.checkIndex(index, size + 1);
        Node<E> newNode = new Node<>(element);
        if (isEmpty()) {
            first = last = newNode;
        } else if (index == 0) {
            addAsHead(newNode);
        } else if (index == size) {
            addAsTail(newNode);
        } else {
            add(index, newNode);
        }
        size++;
    }

    /**
     * Adds a node in the beginning of the list.
     *
     * @param newNode new added Node
     */
    private void addAsHead(Node<E> newNode) {
        Node<E> f = first;
        f.prev = newNode;
        if (f.next == null) {
            last = f;
        }
        first = newNode;
        newNode.next = f;
    }

    /**
     * Adds a node in the end of the list.
     *
     * @param newNode new added Node
     */
    private void addAsTail(Node<E> newNode) {
        Node<E> l = last;
        newNode.prev = l;
        last = newNode;
        l.next = newNode;
    }

    /**
     * Adds a new Node to the specific position in the list.
     *
     * @param index an index of new Node
     * @param newNode new added Node
     */
    private void add(int index, Node<E> newNode) {
        Node<E> node = findNodeByIndex(index - 1);
        Node<E> afterNewNode = node.next;
        afterNewNode.prev = newNode;
        node.next = newNode;
        newNode.prev = node;
        newNode.next = afterNewNode;
    }

    /**
     * Finds and returns Node by index.
     * If it is the last Node then it is returned immediately without iteration.
     *
     * @param index index to find
     * @return Node by index
     */
    private Node<E> findNodeByIndex(int index) {
        if (index == size - 1) {
            return last;
        } else {
            return nodeAt(index);
        }
    }

    /**
     * Finds and returns Node by index.
     *
     * @param index index to find
     * @return Node by index
     */
    private Node<E> nodeAt(int index) {
        Node<E> currentNode = first;
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.next;
        }
        return currentNode;
    }

    /**
     * Changes the value of a list element at specific position. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index   a position of element to change
     * @param element a new element value
     */
    @Override
    public void set(int index, E element) {
        Objects.checkIndex(index, size);
        Node<E> node = findNodeByIndex(index);
        node.item = element;
    }

    /**
     * Retrieves an element by its position index. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index element index
     * @return an element value
     */
    @Override
    public E get(int index) {
        Objects.checkIndex(index, size);
        Node<E> node = findNodeByIndex(index);
        return node.item;
    }

    /**
     * Returns the first element of the list. Operation is performed in constant time O(1)
     *
     * @return the first element of the list
     * @throws java.util.NoSuchElementException if list is empty
     */
    public E getFirst() {
        if (isEmpty()) throw new NoSuchElementException("List is empty");
        return first.item;
    }

    /**
     * Returns the last element of the list. Operation is performed in constant time O(1)
     *
     * @return the last element of the list
     * @throws java.util.NoSuchElementException if list is empty
     */
    public E getLast() {
        if (isEmpty()) throw new NoSuchElementException("List is empty");
        return last.item;
    }

    /**
     * Removes an elements by its position index. In case provided index in out of the list bounds it
     * throws {@link IndexOutOfBoundsException}
     *
     * @param index element index
     * @return deleted element
     */
    @Override
    public E remove(int index) {
        Node<E> prevNode;
        Node<E> nextNode;
        Objects.checkIndex(index, size);
        Node<E> node = findNodeByIndex(index);
        E element = node.item;
        if (size == 1) {
            first = last = null;
            size = 0;
            return element;
        }
        if (node == first) {
            nextNode = node.next;
            nextNode.prev = null;
            first = nextNode;
        } else if (node == last) {
            prevNode = node.prev;
            prevNode.next = null;
            last = prevNode;
        } else {
            prevNode = node.prev;
            nextNode = node.next;
            prevNode.next = nextNode;
            nextNode.prev = prevNode;
        }
        size--;

        return element;
    }


    /**
     * Checks for existing of a specific element in the list.
     *
     * @param element is element to find
     * @return If element exists method returns true, otherwise it returns false
     */
    @Override
    public boolean contains(E element) {
        if (isEmpty()) return false;
        Node<E> node = first;
        if (first.item == element) return true;
        for (int i = 0; i < size; i++) {
            node = node.next;
            if (node == null) return false;
            if (node.item == element) return true;
        }
        return false;
    }

    /**
     * Checks if a list is empty
     *
     * @return {@code true} if list is empty, {@code false} otherwise
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns the number of elements in the list
     *
     * @return number of elements
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Removes all list elements
     */
    @Override
    public void clear() {
        if (isEmpty()) return;
        first = last = null;
        size = 0;
    }

    /**
     * Creates an instance of {@link Iter} who implements {@link Iterator}
     *
     * @return an instance of {@link Iter}
     */
    @Override
    public Iterator<E> iterator() {
        return new Iter();
    }

    /**
     * Nested static class that stores its own element and links to the previous and next Nodes
     */
    private static class Node<E> {
        /**
         * The element is stored here
         */
        private E item;
        /**
         * Link to the next Node
         */
        private Node<E> next;
        /**
         * Link to the previous Node
         */
        private Node<E> prev;

        public Node(E item) {
            this.item = item;
        }

        public Node(Node<E> prev, E item, Node<E> next) {
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
    }

    /**
     * Implementation of interface {@link Iterator}
     */
    private class Iter implements Iterator<E> {
        Node<E> cursor = first;
        Node<E> lastRet;
        int nextIndex;

        /**
         * Checks for the existence of the next element
         */
        @Override
        public boolean hasNext() {
            return cursor != null;
        }

        /**
         * Returns next element otherwise throw {@link NoSuchElementException}
         */
        @Override
        public E next() {
            if (isEmpty()) throw new NoSuchElementException("List is empty");
            if (cursor == null) throw new NoSuchElementException("No more elements");

            lastRet = cursor;
            cursor = cursor.next;
            nextIndex++;

            return lastRet.item;
        }

        /**
         * Unlink and remove current Node
         */
        @Override
        public void remove() {
            if (lastRet == null)
                throw new NoSuchElementException("No more elements or method called twice without next()");

            MyLinkedList.Node<E> lastNext = lastRet.next;
            unlink(lastRet);
            if (cursor == lastRet) {
                cursor = lastNext;
            } else {
                nextIndex--;
            }

            lastRet = null;
        }

        /**
         * Unlink the node
         */
        private void unlink(Node<E> node) {
            Node<E> prev = node.prev;
            Node<E> next = node.next;

            if (prev == null) {
                first = next;
            } else {
                prev.next = next;
                node.prev = null;
            }

            if (next == null) {
                last = prev;
            } else {
                next.prev = prev;
                node.next = null;
            }

            node.item = null;
            size--;
        }
    }
}
